//
//  BrochureCollectionViewCell.h
//  Bonial
//
//  Created by A. J. on 29/02/16.
//  Copyright © 2016 AJ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Brochure.h"

@interface BrochureCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UILabel *retailerName;
@property (weak, nonatomic) IBOutlet UIImageView *image;

-(void)configureCellWithBrochure:(Brochure *)brochure;

@end
