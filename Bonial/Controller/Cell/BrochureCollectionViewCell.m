//
//  BrochureCollectionViewCell.m
//  Bonial
//
//  Created by A. J. on 29/02/16.
//  Copyright © 2016 AJ. All rights reserved.
//

#import "BrochureCollectionViewCell.h"
#import "BonialManager.h"

@implementation BrochureCollectionViewCell
-(void)configureCellWithBrochure:(Brochure *)brochure{
    self.title.text = brochure.title;
    self.retailerName.text = brochure.retailerName;
    self.image.image = nil;
    [[BonialManager sharedManager] fetchImageFromUrl:brochure.coverUrl onDidLoad:^(id imageData) {
        dispatch_async(dispatch_get_main_queue(), ^{
            self.image.image = imageData ? [UIImage imageWithData: imageData] : [UIImage imageNamed:@"noimg"];
        });
    }];

}

@end
