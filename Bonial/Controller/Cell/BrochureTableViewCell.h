//
//  BrochureTableViewCell.h
//  Bonial
//
//  Created by A. J. on 01/03/16.
//  Copyright © 2016 AJ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CollectionViewDataSource.h"

@interface BrochureTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (strong, nonatomic) CollectionViewDataSource *dataSource;

-(void)configureCellWithBrochures:(NSArray *)brochures;

@end
