//
//  BrochureTableViewCell.m
//  Bonial
//
//  Created by A. J. on 01/03/16.
//  Copyright © 2016 AJ. All rights reserved.
//

#import "BrochureTableViewCell.h"
#import "BrochureCollectionViewCell.h"

@implementation BrochureTableViewCell

-(void)configureCellWithBrochures:(NSArray *)brochures{
    [self setupCollectionViewWithBrochures:brochures];
}


- (void)setupCollectionViewWithBrochures:(NSArray *)brochures{
    
    self.dataSource = [[CollectionViewDataSource alloc] initWithBrochures: brochures cellIdentifier:@"BrochureCollectionViewCellId" configureCellBlock:^(id cell, id item) {
        [(BrochureCollectionViewCell *)cell configureCellWithBrochure:(Brochure *)item];
    }];
    
    self.collectionView.dataSource = self.dataSource;
    self.collectionView.backgroundColor = [UIColor clearColor];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:0 inSection:0];
    [self.collectionView scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:NO];

}

@end
