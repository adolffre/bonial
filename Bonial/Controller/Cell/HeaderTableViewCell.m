//
//  HeaderTableViewCell.m
//  Bonial
//
//  Created by A. J. on 01/03/16.
//  Copyright © 2016 AJ. All rights reserved.
//

#import "HeaderTableViewCell.h"
#import "BonialManager.h"

@implementation HeaderTableViewCell

-(void)configureCellWithSector:(Sector *)sector{
    self.name.text = sector.name;
    self.counter.text = [NSString stringWithFormat:@"%lu itens",[[sector.brochures allObjects] count]];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.image.image = nil;
    [[BonialManager sharedManager] fetchImageFromUrl:sector.urlImage onDidLoad:^(id imageData) {
        dispatch_async(dispatch_get_main_queue(), ^{
            self.image.image = [UIImage imageWithData: imageData];
        });
    }];
    
}
@end
