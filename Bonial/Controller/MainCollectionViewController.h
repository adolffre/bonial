//
//  MainCollectionViewController.h
//  Bonial
//
//  Created by A. J. on 29/02/16.
//  Copyright © 2016 AJ. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef enum : NSUInteger {
    HeaderCell ,
    BrochureCell
} RowType;
@interface MainCollectionViewController : UIViewController

@end
