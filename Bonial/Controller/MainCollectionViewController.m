//
//  MainCollectionViewController.m
//  Bonial
//
//  Created by A. J. on 29/02/16.
//  Copyright © 2016 AJ. All rights reserved.
//

#import "MainCollectionViewController.h"
#import "CollectionViewDataSource.h"
#import "DatabaseManager.h"
#import "BrochureCollectionViewCell.h"
#import "TableViewDataSource.h"
#import "BrochureTableViewCell.h"
#import "HeaderTableViewCell.h"
#import "BonialManager.h"

static CGFloat const headerHeight = 56.f;
static CGFloat const brochureHeight = 230.f;

@interface MainCollectionViewController()<UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) CollectionViewDataSource *dataSource;
@property (strong, nonatomic) TableViewDataSource *tableViewDataSource;
@property (strong, nonatomic) NSArray *sectors;

@end

@implementation MainCollectionViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    [[BonialManager sharedManager] getBonialDataWithCompletionHandler:^(BOOL valid, NSError *error) {
        self.sectors = [[DatabaseManager sharedManager] allSectors];
        [self setupTableView];
    } ];
}

-(void)setupTableView{
    
    self.tableViewDataSource = [[TableViewDataSource alloc] initWithSectors:self.sectors configureCellBlock:^(id cell, id itens) {
            if([cell isKindOfClass:[BrochureTableViewCell class]]){
                [(BrochureTableViewCell *)cell configureCellWithBrochures:itens];
            }else{
                [(HeaderTableViewCell *)cell configureCellWithSector: itens];
            }
        }];
    
        self.tableView.dataSource = self.tableViewDataSource;
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.tableView reloadData];
    
    });
    
}

#pragma mark - TableView Delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat height =0;
    switch (indexPath.row) {
        case HeaderCell:{
            height = headerHeight;
            break;
        }
        case BrochureCell:{
            height = brochureHeight;
            break;
        }
        default:
            break;
    }
    return height;
}

@end
