//
//  DatabaseManager.h
//  Bonial
//
//  Created by A. J. on 29/02/16.
//  Copyright © 2016 AJ. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "Brochure+CoreDataProperties.h"
#import "Sector+CoreDataProperties.h"

@interface DatabaseManager : NSObject

#pragma mark - Singleton

+ (instancetype)sharedManager;
+ (instancetype)sharedManagerForTesting;

#pragma mark - Properties

@property (nonatomic) NSManagedObjectContext *context;

#pragma mark - Create

- (Brochure *)newBrochure;
- (Sector *)newSector;

#pragma mark - Read

- (NSArray *)allSectors;

#pragma mark - Update

- (BOOL)save;

#pragma mark - Delete

- (BOOL)removeRecord:(NSManagedObject *)record;
- (BOOL)removeAllSectors;


@end
