//
//  DatabaseManager.m
//  Bonial
//
//  Created by A. J. on 29/02/16.
//  Copyright © 2016 AJ. All rights reserved.
//

#import "DatabaseManager.h"
#import "AppDelegate.h"

@implementation DatabaseManager

#pragma mark - Singleton Methods

+ (instancetype)sharedManager {
    static DatabaseManager *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
        sharedMyManager.context = [(AppDelegate *)[[UIApplication sharedApplication] delegate] managedObjectContext];
    });
    return sharedMyManager;
}

+ (instancetype)sharedManagerForTesting {
    static DatabaseManager *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
        sharedMyManager.context = [(AppDelegate *)[[UIApplication sharedApplication] delegate] managedObjectContextForTesting];
    });
    return sharedMyManager;
}

#pragma mark - Core Data

- (BOOL)saveContext {
    NSError *error;
    if (![self.context save:&error]) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"errorOnSaveCoreDataContext" object:error];
        return NO;
    }
    return YES;
}

- (NSFetchRequest *)newFetchRequestForEntity:(NSString *)entityName {
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:entityName inManagedObjectContext:self.context];
    [fetchRequest setEntity:entity];
    return fetchRequest;
}

- (NSArray *)executeFetchRequest:(NSFetchRequest *)fetchRequest {
    NSError *error;
    NSArray *fetchedObjects = [self.context executeFetchRequest:fetchRequest error:&error];
    if (error) {
        return nil;
    } else {
        return fetchedObjects;
    }
}

- (id)allObjectsForEntity:(NSString *)entityName
             andPredicate:(NSPredicate *)predicate {
    NSFetchRequest *fetchRequest = [self newFetchRequestForEntity:entityName];
    if (predicate) {
        [fetchRequest setPredicate:predicate];
    }
    return [self executeFetchRequest:fetchRequest];
}

- (id)objectForEntity:(NSString *)entityName
         andPredicate:(NSPredicate *)predicate {
    NSArray *fetchedObjects = [self allObjectsForEntity:entityName
                                           andPredicate:predicate];
    if (fetchedObjects) {
        if (fetchedObjects.count > 0) {
            return fetchedObjects.firstObject;
        }
    }
    return nil;
}

- (BOOL)removeAllRecordsFromEntity:(NSString *)entityName {
    NSArray *allRecords = [self allObjectsForEntity:entityName
                                       andPredicate:nil];
    if (allRecords) {
        for (NSManagedObject *record in allRecords) {
            [self removeRecord:record];
        }
    }
    return YES;
}

#pragma mark - Create

- (Brochure *)newBrochure{
    Brochure *record = (Brochure *)[NSEntityDescription insertNewObjectForEntityForName:@"Brochure" inManagedObjectContext:self.context];
    return record;
}

- (Sector *)newSector{
    Sector *record = (Sector *)[NSEntityDescription insertNewObjectForEntityForName:@"Sector" inManagedObjectContext:self.context];
    return record;
}

#pragma mark - Read

- (NSArray *)allSectors {
    NSFetchRequest *fetchRequest = [self newFetchRequestForEntity:@"Sector"];
    [fetchRequest setSortDescriptors:@[[[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES]]];
    
    return [self executeFetchRequest:fetchRequest];
}

#pragma mark - Update

- (BOOL)save {
    return [self saveContext];
}

#pragma mark - Delete

- (BOOL)removeRecord:(NSManagedObject *)record {
    [self.context deleteObject:record];
    return YES;
}

- (BOOL)removeAllSectors {
    [self removeAllRecordsFromEntity:@"Brochure"];
    return [self removeAllRecordsFromEntity:@"Sector"];
}

@end
