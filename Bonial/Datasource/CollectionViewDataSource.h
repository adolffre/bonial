//
//  CollectionViewDataSource.h
//  Bonial
//
//  Created by A. J. on 29/02/16.
//  Copyright © 2016 AJ. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef void (^CollectionViewCellConfigureBlock)(id cell, id item);
@interface CollectionViewDataSource : NSObject <UICollectionViewDataSource>

- (id)initWithBrochures:(NSArray *)brochures
         cellIdentifier:(NSString *)aCellIdentifier
     configureCellBlock:(CollectionViewCellConfigureBlock)aConfigureCellBlock;

- (id)itemAtIndexPath:(NSIndexPath *)indexPath;
    
@end
