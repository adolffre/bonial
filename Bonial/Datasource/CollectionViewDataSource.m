//
//  ArrayDataSource.m
//  Bonial
//
//  Created by A. J. on 29/02/16.
//  Copyright © 2016 AJ. All rights reserved.
//

#import "CollectionViewDataSource.h"
#import "Sector.h"

@interface CollectionViewDataSource()
@property (nonatomic, strong) NSArray *brochures;
@property (nonatomic, copy) NSString *cellIdentifier;
@property (nonatomic, copy) CollectionViewCellConfigureBlock configureCellBlock;

@end
@implementation CollectionViewDataSource

- (id)init
{
    return nil;
}

- (id)initWithBrochures:(NSArray *)brochures
         cellIdentifier:(NSString *)aCellIdentifier
     configureCellBlock:(CollectionViewCellConfigureBlock)aConfigureCellBlock
{
    self = [super init];
    if (self) {
        self.brochures = brochures;
        self.cellIdentifier = aCellIdentifier;
        self.configureCellBlock = [aConfigureCellBlock copy];
        
    }
    return self;
}

- (id)itemAtIndexPath:(NSIndexPath *)indexPath
{
    return [self.brochures objectAtIndex:indexPath.row];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return [self.brochures count];
    
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"BrochureCollectionViewCellId" forIndexPath:indexPath];
    
    id item = [self itemAtIndexPath:indexPath];
    self.configureCellBlock(cell, item);
    return cell;
    
}


@end
