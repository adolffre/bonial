//
//  TableViewDataSource.h
//  Bonial
//
//  Created by A. J. on 01/03/16.
//  Copyright © 2016 AJ. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void (^TableViewCellConfigureBlock)(id cell, id itens);

@interface TableViewDataSource : NSObject <UITableViewDataSource>

- (id)initWithSectors:(NSArray *)sectors
     configureCellBlock:(TableViewCellConfigureBlock)aConfigureCellBlock;

- (id)itemAtIndexPath:(NSIndexPath *)indexPath;

@end
