//
//  TableViewDataSource.m
//  Bonial
//
//  Created by A. J. on 01/03/16.
//  Copyright © 2016 AJ. All rights reserved.
//

#import "TableViewDataSource.h"
#import "Sector.h"

@interface TableViewDataSource()<UITableViewDataSource>

@property (nonatomic, strong) NSArray *sectors;
@property (nonatomic, copy) NSString *cellIdentifier;
@property (nonatomic, copy) TableViewCellConfigureBlock configureCellBlock;
@end

@implementation TableViewDataSource

- (id)init
{
    return nil;
}

- (id)initWithSectors:(NSArray *)sectors
     configureCellBlock:(TableViewCellConfigureBlock)aConfigureCellBlock{
    self = [super init];
    if (self) {
        self.sectors = sectors;
        self.configureCellBlock = [aConfigureCellBlock copy];
    }
    return self;
}

- (id)itemAtIndexPath:(NSIndexPath *)indexPath
{
    Sector *sector = self.sectors[indexPath.section];
    NSArray *allBrochures = [sector.brochures allObjects];
    return allBrochures;
}
-(id)sectorAtIndex:(NSInteger)index{
    return self.sectors[index];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 2;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.sectors.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell;
    if(indexPath.row == 0){
        cell = [tableView dequeueReusableCellWithIdentifier:@"HeaderTableViewCellId" forIndexPath:indexPath];
        if(cell != nil){
            Sector *sector = [self sectorAtIndex:indexPath.section];
            self.configureCellBlock(cell,sector);
        }
    }else {
        cell = [tableView dequeueReusableCellWithIdentifier:@"BrochureTableViewCellId" forIndexPath:indexPath];
        if(cell != nil){
            id brochures = [self itemAtIndexPath:indexPath];
            self.configureCellBlock(cell, brochures);
        }
    }
    return cell;

}

@end
