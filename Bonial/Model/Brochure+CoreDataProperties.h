//
//  Brochure+CoreDataProperties.h
//  Bonial
//
//  Created by A. J. on 29/02/16.
//  Copyright © 2016 AJ. All rights reserved.
//


#import "Brochure.h"

NS_ASSUME_NONNULL_BEGIN

@interface Brochure (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *brochureId;
@property (nullable, nonatomic, retain) NSString *coverUrl;
@property (nullable, nonatomic, retain) NSString *retailerName;
@property (nullable, nonatomic, retain) NSString *title;
@property (nullable, nonatomic, retain) Sector *sector;

@end

NS_ASSUME_NONNULL_END
