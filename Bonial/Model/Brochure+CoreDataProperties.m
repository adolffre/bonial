//
//  Brochure+CoreDataProperties.m
//  Bonial
//
//  Created by A. J. on 29/02/16.
//  Copyright © 2016 AJ. All rights reserved.
//


#import "Brochure+CoreDataProperties.h"

@implementation Brochure (CoreDataProperties)

@dynamic brochureId;
@dynamic coverUrl;
@dynamic retailerName;
@dynamic title;
@dynamic sector;

@end
