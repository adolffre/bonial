//
//  Brochure.h
//  Bonial
//
//  Created by A. J. on 29/02/16.
//  Copyright © 2016 AJ. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Sector;

NS_ASSUME_NONNULL_BEGIN

@interface Brochure : NSManagedObject

-(void )startBrochureFromDictionary:(NSDictionary *)dictionary;

@end

NS_ASSUME_NONNULL_END

#import "Brochure+CoreDataProperties.h"
