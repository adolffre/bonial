//
//  Brochure.m
//  Bonial
//
//  Created by A. J. on 29/02/16.
//  Copyright © 2016 AJ. All rights reserved.
//

#import "Brochure.h"
#import "Sector.h"

@implementation Brochure

-(void )startBrochureFromDictionary:(NSDictionary *)dictionary{
    self.brochureId = [[dictionary objectForKey:@"id"] stringValue];
    self.coverUrl = [dictionary objectForKey:@"coverUrl"];
    self.retailerName = [dictionary objectForKey:@"retailerName"];
    self.title = [dictionary objectForKey:@"title"];
}

@end
