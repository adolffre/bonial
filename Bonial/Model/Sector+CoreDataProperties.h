//
//  Sector+CoreDataProperties.h
//  Bonial
//
//  Created by A. J. on 29/02/16.
//  Copyright © 2016 AJ. All rights reserved.
//


#import "Sector.h"

NS_ASSUME_NONNULL_BEGIN

@interface Sector (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *name;
@property (nullable, nonatomic, retain) NSString *sectorId;
@property (nullable, nonatomic, retain) NSString *urlImage;
@property (nullable, nonatomic, retain) NSSet<Brochure *> *brochures;

@end

@interface Sector (CoreDataGeneratedAccessors)

- (void)addBrochuresObject:(Brochure *)value;
- (void)removeBrochuresObject:(Brochure *)value;
- (void)addBrochures:(NSSet<Brochure *> *)values;
- (void)removeBrochures:(NSSet<Brochure *> *)values;

@end

NS_ASSUME_NONNULL_END
