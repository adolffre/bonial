//
//  Sector+CoreDataProperties.m
//  Bonial
//
//  Created by A. J. on 29/02/16.
//  Copyright © 2016 AJ. All rights reserved.
//


#import "Sector+CoreDataProperties.h"

@implementation Sector (CoreDataProperties)

@dynamic name;
@dynamic sectorId;
@dynamic urlImage;
@dynamic brochures;

@end
