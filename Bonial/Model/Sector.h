//
//  Sector.h
//  Bonial
//
//  Created by A. J. on 29/02/16.
//  Copyright © 2016 AJ. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Brochure;

NS_ASSUME_NONNULL_BEGIN

@interface Sector : NSManagedObject

-(NSArray *)startSectorFromDictionary:(NSDictionary *)dictionary;
@end

NS_ASSUME_NONNULL_END

#import "Sector+CoreDataProperties.h"
