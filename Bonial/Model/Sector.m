//
//  Sector.m
//  Bonial
//
//  Created by A. J. on 29/02/16.
//  Copyright © 2016 AJ. All rights reserved.
//

#import "Sector.h"
#import "Brochure.h"

@implementation Sector

-(NSArray *)startSectorFromDictionary:(NSDictionary *)dictionary{
    self.name = [dictionary objectForKey:@"name"];
    self.sectorId = [[dictionary objectForKey:@"id"] stringValue];
    self.urlImage = [dictionary objectForKey:@"url"];
    return (NSArray *)[dictionary objectForKey:@"brochures"];
}
@end
