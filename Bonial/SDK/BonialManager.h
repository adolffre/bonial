//
//  BonialManager.h
//  Bonial
//
//  Created by A. J. on 29/02/16.
//  Copyright © 2016 AJ. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DatabaseManager.h"

@interface BonialManager : NSObject
/**
 *  All Sectors completion
 *
 *  @param valid             Bool if resquest was successful
 *  @param error             Error for failure
 */
typedef void (^GetAllSectors)(BOOL valid, NSError *error);

/**
 *  Download Image completion
 *
 *  @param id             image if successful or nil if not
 */
typedef void (^ImageDidLoadBlock)(id imageData);

#pragma mark - Properties
/**
 *  Api url
 */
@property (nonatomic) NSString *apiUrl;
@property (nonatomic) DatabaseManager *database;

#pragma mark - Singleton

+ (instancetype)sharedManager;
+ (instancetype)sharedManagerForTest;

#pragma mark - Bonial
/**
 *  Get all Sectors and brochures
 *
 *  @param completion as above
 */
- (void)getBonialDataWithCompletionHandler:(GetAllSectors)completion;

/**
 *  Get image from url
 *
 *  @param urlString    Url String of image
 *  @param completion as above
 */
- (void)fetchImageFromUrl:(NSString*)urlString onDidLoad:(ImageDidLoadBlock)onImageDidLoad;
@end
