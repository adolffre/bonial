//
//  BonialManager.m
//  Bonial
//
//  Created by A. J. on 29/02/16.
//  Copyright © 2016 AJ. All rights reserved.
//

#import "BonialManager.h"

@implementation BonialManager


#pragma mark - Singleton Methods

+ (instancetype)sharedManager {
    static BonialManager *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
        sharedMyManager.apiUrl = @"https://dl.dropboxusercontent.com/u/41357788/coding_task/api.json";
        sharedMyManager.database = [DatabaseManager sharedManager];
    });
    return sharedMyManager;
}

+ (instancetype)sharedManagerForTest {
    static BonialManager *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
        sharedMyManager.apiUrl = @"https://dl.dropboxusercontent.com/u/41357788/coding_task/api.json";
        sharedMyManager.database = [DatabaseManager sharedManagerForTesting];
    });
    return sharedMyManager;
}

#pragma mark - Requests

- (void)getBonialDataWithCompletionHandler:(GetAllSectors)completion {
    
    // Request
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setHTTPMethod:@"GET"];
    [request setURL:[NSURL URLWithString:self.apiUrl]];
    
    // Execute Request
    NSURLSessionDataTask *task = [[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSHTTPURLResponse *httpResponse =(NSHTTPURLResponse *)response;
        if(error || httpResponse.statusCode !=200){
            completion(nil,error);
        }else{
            completion([self parseAndSaveSectionJsonFromData:data],nil);
        }

    }];
    [task resume];
}

-(BOOL)parseAndSaveSectionJsonFromData:(NSData *)data{
    NSError *err = nil;
    NSDictionary *jsonDictionary = [NSJSONSerialization JSONObjectWithData:data
                                                         options:NSJSONReadingMutableContainers
                                                           error:&err];
    if (jsonDictionary) {

        for (NSDictionary *sectorDictionary in [jsonDictionary objectForKey:@"sectors"]) {
            
            Sector *sector = [self.database newSector];
            NSArray *brochureArray = [sector startSectorFromDictionary:sectorDictionary];
            
            NSMutableArray *brochures = [NSMutableArray new];
            
            for (NSDictionary *brochureDictionary in brochureArray) {
                Brochure *brochure = [self.database newBrochure];
                [brochure startBrochureFromDictionary:brochureDictionary];
                [brochure setSector:sector];
                [brochures addObject:brochure];
            }
        
            sector.brochures = [NSSet setWithArray:brochures];
        }
    
        return [self.database save];
    } else {
        return NO;
    }
    
}

- (void)fetchImageFromUrl:(NSString*)urlString onDidLoad:(ImageDidLoadBlock)onImageDidLoad{
    dispatch_async(dispatch_get_global_queue(0,0), ^{
        NSData * data = [[NSData alloc] initWithContentsOfURL: [NSURL URLWithString: urlString]];
            onImageDidLoad(data ? data : nil);
    });
    
}

@end
