//
//  BonialManagerTests.m
//  BonialManagerTests
//
//  Created by A. J. on 29/02/16.
//  Copyright © 2016 AJ. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "BonialManager.h"

@interface BonialManagerTests : XCTestCase

@end

@implementation BonialManagerTests

- (void)setUp {
    [super setUp];
}

- (void)tearDown {
    [super tearDown];
}

#pragma mark - Singleton

- (void)testManagerSingleton {
    
    // Singleton
    XCTAssertNotNil([BonialManager sharedManagerForTest]);
    XCTAssertEqualObjects([BonialManager sharedManagerForTest], [BonialManager sharedManagerForTest]);
    
    // API URL
    XCTAssertNotNil([[BonialManager sharedManagerForTest] apiUrl]);
    
}

#pragma mark - Requests

- (void)testGetBonialDataWithCompletionHandler {
    
    BonialManager *manager = [BonialManager sharedManagerForTest];
    
    XCTestExpectation *expectation = [self expectationWithDescription:@"testGetDataWithGoodParameters"];
    
    [manager getBonialDataWithCompletionHandler:^(BOOL valid, NSError *error) {
        XCTAssertTrue(valid);
        [expectation fulfill];
    }];
    
    [self waitForExpectationsWithTimeout:10.0 handler:^(NSError *error) {
        XCTAssertNil(error);
    }];
    
}

-(void)testFetchImageFromUrl{
    BonialManager *manager = [BonialManager sharedManagerForTest];
    
    XCTestExpectation *expectation = [self expectationWithDescription:@"testGetDataWithGoodParameters"];
    [manager fetchImageFromUrl:@"http://static06.retale.com/brochures/0000/0000/0000/5237/52370261/images/320x480/page_0.jpg" onDidLoad:^(id imageData) {
        XCTAssertTrue(imageData);
        [expectation fulfill];
    }];
    [self waitForExpectationsWithTimeout:10.0 handler:^(NSError *error) {
        XCTAssertNil(error);
    }];
}

@end
